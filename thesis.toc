\boolfalse {citerequest}\boolfalse {citetracker}\boolfalse {pagetracker}\boolfalse {backtracker}\relax 
\defcounter {refsection}{0}\relax 
\select@language {french}
\defcounter {refsection}{0}\relax 
\select@language {french}
\defcounter {refsection}{0}\relax 
\contentsline {chapter}{Preface}{i}{Doc-Start}
\defcounter {refsection}{0}\relax 
\contentsline {section}{Remerciements}{i}{section*.1}
\defcounter {refsection}{0}\relax 
\contentsline {chapter}{\numberline {1}Introduction}{1}{chapter.1}
\defcounter {refsection}{0}\relax 
\contentsline {chapter}{\numberline {2}Contexte}{2}{chapter.2}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {2.1}Pictawall}{2}{section.2.1}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {2.1.1}Les produits}{2}{subsection.2.1.1}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {2.1.2}L'équipe}{2}{subsection.2.1.2}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {2.2}Environnement technique}{3}{section.2.2}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {2.2.1}Technologies}{3}{subsection.2.2.1}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {2.2.2}Logiciels}{4}{subsection.2.2.2}
\defcounter {refsection}{0}\relax 
\contentsline {chapter}{\numberline {3}Politique de développement}{5}{chapter.3}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {3.1}Les transpilers}{6}{section.3.1}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {3.2}Les source maps}{7}{section.3.2}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {3.3}Les polyfills}{8}{section.3.3}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {3.3.1}Les polyfills JavaScript}{8}{subsection.3.3.1}
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{Isomorphisme}{9}{subsubsection*.3}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {3.3.2}Le futur des polyfills}{10}{subsection.3.3.2}
\defcounter {refsection}{0}\relax 
\contentsline {chapter}{\numberline {4}L'application}{11}{chapter.4}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {4.1}L'application existante}{11}{section.4.1}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {4.1.1}Le Social Hub}{11}{subsection.4.1.1}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {4.1.2}La plateforme}{14}{subsection.4.1.2}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {4.2}Objectifs du stage}{16}{section.4.2}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {4.3}Déroulement du stage}{17}{section.4.3}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {4.3.1}Semaine 1}{17}{subsection.4.3.1}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {4.3.2}Semaine 2}{17}{subsection.4.3.2}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {4.3.3}Semaine 3}{17}{subsection.4.3.3}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {4.3.4}Semaine 4}{17}{subsection.4.3.4}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {4.3.5}Semaine 5 et 6}{17}{subsection.4.3.5}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {4.3.6}Semaine 7}{18}{subsection.4.3.6}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {4.3.7}Semaine 8}{18}{subsection.4.3.7}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {4.3.8}Semaine 9}{18}{subsection.4.3.8}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {4.3.9}Semaine 10}{18}{subsection.4.3.9}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {4.3.10}Semaine 11}{19}{subsection.4.3.10}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {4.3.11}Semaine 12}{19}{subsection.4.3.11}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {4.3.12}Semaine 13}{20}{subsection.4.3.12}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {4.3.13}Semaine 14}{20}{subsection.4.3.13}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {4.3.14}Semaine 15}{20}{subsection.4.3.14}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {4.3.15}Semaine 16}{20}{subsection.4.3.15}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {4.4}L'application finale}{21}{section.4.4}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {4.4.1}Simplification d'intégration}{21}{subsection.4.4.1}
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{Le script de boot}{21}{subsubsection*.4}
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{Les dépendances}{23}{subsubsection*.5}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {4.4.2}Suppression de l'iframe}{26}{subsection.4.4.2}
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{Les collisions en CSS}{26}{subsubsection*.6}
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{Rendre le design Responsive}{27}{subsubsection*.7}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {4.4.3}Optimisation}{30}{subsection.4.4.3}
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{Suppression des dépendances}{30}{subsubsection*.8}
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{Optimisation du code source}{33}{subsubsection*.9}
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{Réimplémentation du SDK}{35}{subsubsection*.10}
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{Chargement des \glspl {asset}}{36}{subsubsection*.11}
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{Résultat final}{38}{subsubsection*.12}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {4.4.4}Personnalisation}{40}{subsection.4.4.4}
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{Configuration des fonctionnalités}{40}{subsubsection*.13}
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{Configuration du style}{41}{subsubsection*.14}
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{Ajout de fonctionnalités}{42}{subsubsection*.15}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {4.4.5}Internationalization (i18n)}{45}{subsection.4.4.5}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {4.4.6}Traçage}{46}{subsection.4.4.6}
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{Envoi des données}{46}{subsubsection*.16}
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{Collecte}{47}{subsubsection*.17}
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{Identification des données}{47}{subsubsection*.18}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {4.4.7}Sécurité}{49}{subsection.4.4.7}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {4.4.8}Stabilité}{50}{subsection.4.4.8}
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{Consolider les données}{50}{subsubsection*.19}
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{Consolider le code source}{50}{subsubsection*.20}
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{Rattrapage des erreurs}{53}{subsubsection*.21}
\defcounter {refsection}{0}\relax 
\contentsline {chapter}{\numberline {5}Conclusion}{54}{chapter.5}
\defcounter {refsection}{0}\relax 
\contentsline {chapter}{Acronymes}{a}{section*.22}
\defcounter {refsection}{0}\relax 
\contentsline {chapter}{Glossaire}{b}{section*.24}
\defcounter {refsection}{0}\relax 
\contentsline {chapter}{Bibliographie}{e}{chapter*.25}
\defcounter {refsection}{0}\relax 
\contentsline {chapter}{Annexes}{g}{Annexe*.27}
\contentsfinish 
