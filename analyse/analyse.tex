\chapter{Politique de développement}
\label{chapter:analysis}

L'informatique est un domaine évoluant extrêmement vite, de nouvelles technologies sont continuellement créées afin de faciliter la tâche du développeur. 
Le monde du développement web n'en est pas exclu. Autrefois dans un état stagnant, il est aujourd'hui l'un des domaines avec l'avancée la plus rapide :

En 2015, après 6 ans sans mise à jour majeure, L'organisation ECMA International a publié la sixième édition d'\gls{ecmascript}, le standard derrière JavaScript, nommée \emph{\gls{ecmascript} 2015}. Elle a également changé le système derrière le développement du standard. Désormais, les individus non membre du comité en charge d'\gls{ecmascript} ont la possibilité de contribuer au langage en développant de nouvelles fonctionnalités et en les présentant au comité. De plus, Commençant avec \gls{ecmascript} 2016, le standard sera publié annuellement et inclura les propositions ayant atteint la dernière étape de leur développement et ayant été approuvées par le comité.\autocite{ECMA:1} \footnote{Le comité se réserve cependant le droit de délayer la publication de certaines nouvelles fonctionnalités même si celles-ci sont en dernière étape de développement.}

Le JavaScript n'est pas la seule technologie web standard à évoluer. Les navigateurs implémentent une série de spécifications et d'\gls{api} permettant au développeur de créer des applications web riches. 
D'un point de vue historique, c'est le \glsdisp{w3c}{World Wide Web Consortium (W3C)} qui est en charge du développement des standards du web. Ces standards incluent l'XML, l'(x)HTML et le CSS; pour ne nommer que les plus connus.

Ces spécifications étaient, à l'époque, des documents écrits sur plusieurs années puis publiés dans leur version finale en une fois. Cependant, un désaccord entre le consortium et certains développeurs de navigateurs conduisit à la création d'un nouveau groupe, le \gls{whatwg}. Ce dernier prit en charge le développement de la version 5 d'HTML, ainsi que d'autres spécifications mineures.\autocite{HTML:1} Leur philosophie au niveau de la publication de leur standard varie fortement de celle du \gls{w3c}: Leurs documents ne sont désormais plus publiés dans leur version finale. À la place, le \gls{whatwg} publie ce qu'ils appellent des "Living Standards". Ces standards sont publiés dans un état fonctionnel et complet, mais continuent d'évoluer malgré tout.

Les navigateurs modernes suivent aussi ce principe de cycles de développement rapides, les nouvelles fonctionnalités spécifiées dans les différents standards sont donc de plus en plus rapidement disponibles pour les utilisateurs finaux. Par exemple, aucun des navigateurs populaires d'aujourd'hui ne supporte moins de 90\% des fonctionnalités d'\gls{ecmascript} 2015.\autocite{ECMA:2}

Il reste malgré tout un dernier point posant problème, empêchant les développeurs d'utiliser les dernières technologies: La vitesse de mise à jour des navigateurs par les utilisateurs. Nombreux sont les utilisateurs utilisant encore Internet Explorer 11 ou Safari 7. Ces logiciels sont anciens, non supportés et n'implémentent certainement pas les dernières versions des spécifications. Les supporter signifie que le développeur se doit de se limiter aux anciennes fonctionnalités dont les \glspl{api} sont souvent complexes --- XMLHttpRequest par exemple --- ou simplement inexistantes --- la syntaxe des classes en \gls{ecmascript} 6 en est une. Ne pas les supporter signifie la perte de clients potentiels.

Fort heureusement, la communauté autour de JavaScript est large et créative. Au cours des années, des outils se sont développés pour permettre aux développeurs d'utiliser les technologies web de pointe tout en supportant un maximum de navigateurs, neufs ou anciens.

\section{Les transpilers}

Les \emph{\glspl{transpiler}} sont des outils capable de lire un code source, et de ré-écrire ce code source en son équivalent dans un autre langage. Les navigateurs ne comprenant que le JavaScript, les \glspl{transpiler} peuvent être utilisés pour permettre aux développeurs d'écrire leur application dans le langage de leur choix --- par exemple JSX, CoffeeScript, ou encore TypeScript --- et d'ensuite la compiler en JavaScript.

Mais la capacité des \glspl{transpiler} ne se limite pas à convertir du Java en PHP, il est également possible de les utiliser pour ré-écrire une application utilisant une version d'un langage --- ici, \gls{ecmascript} 6 --- en une autre version de ce même langage --- \gls{ecmascript} 5.

Il existe une multitude de transpilers capable d'effectuer cette tâche ; les plus populaires actuellement étant le projet Traceur de Google, et Babel. Les deux sont fortement similaires mais Babel possède l'avantage de la modularité: Il faut lui préciser la liste des fonctionnalités qu'il doit transformer. Par exemple, pour activer le support pour les nouveaux mot-clés \texttt{let} et \texttt{const}, il faut activer le plugin \texttt{transform-es2015-block-scoping} dans sa configuration. Cette modularité permet de progressivement désactiver la transpilation des fonctionnalités au fur et à mesure que leur support dans les navigateurs grandit. Bien que cela soit plus risqué, elle permet aussi de choisir d'utiliser les fonctionnalités encore en développement, prévues pour les futures versions d'\gls{ecmascript}. On peut par activer la transpilation pour \texttt{async/await}, une (potentielle) future fonctionnalité simplifiant la gestion de appels asynchrones.

Il est, de cette façon, toujours possible d'utiliser la dernière version d'\gls{ecmascript} sans avoir à se soucier du support des navigateurs. Nous pouvons néanmoins profiter des implémentations natives une fois le support des fonctionnalités plus répandu, et ce sans devoir modifier une ligne du code source.

\section{Les source maps}

Transpiler a pour conséquence que le code exécuté par le moteur JavaScript n'est pas celui écrit par le développeur. Lorsque ce dernier souhaite utiliser les fonctionnalités de débug de son navigateur ou de node.js, il sera donc confronté à un code différent de celui qu'il peut voir dans son éditeur de texte. Bien que Babel ait tendance à générer un code lisible et fort similaire à celui source, il peut être intéressant d'installer un mécanisme permettant aux outils de débug d'effectuer la traduction entre le code compilé et celui d'origine : Les source maps.

Les Source Maps ne sont pas spécifiques à JavaScript ni à la transpilation, elles sont utiles pour toute sorte de manipulation effectuée automatiquement sur le code. Ainsi, il n'est pas rare de les utiliser dans les fichiers CSS, ou pour faire le lien entre les versions minifiées et celles de développement des distribuables par exemple.

Le fonctionnement de cette technologie n'est pas encore standard, mais la majorité des outils utilisent le format défini dans "Source Maps Revision 3 Proposal"\autocite{sourcemaps:1}.

En temps normal, et ce à moins de vouloir implémenter une application exploitant les Source Maps, le développeur n'a pas à se soucier de leur fonctionnement. Leur création est souvent effectuée en activant un simple paramètre dans la configuration de l'outil générant du code. Babel, par exemple, demande de définir la valeur \texttt{sourceMaps} à \texttt{"inline"} dans le fichier \texttt{.babelrc} à la base du projet dans lequel celui-ci va tourner. Similairement, utiliser les source maps dans les outils de débug ne demande également que d'activer une options dans la configuration de ces outils.

\section{Les polyfills}

Si les transpilers permettent de combler les manquements dans le support d'un langage, les polyfills sont, eux, conçus pour combler les manquements dans la librairie standard. Plus spécifiquement, leur objectif n'est pas d'ajouter de nouvelles fonctionnalités mais d'implémenter les \glspl{api} standards afin de les rendre disponibles dans les navigateurs ne les ayant pas implémentées nativement.

\subsection{Les polyfills JavaScript}

JavaScript étant un langage flexible, ajouter des fonctionnalités à sa librairie standard est aisé. Ajouter des méthodes à une classe est aussi simple que d'assigner une nouvelle fonction à l'une des propriétés du prototype de la classe.

Ainsi pour ajouter la méthode \texttt{includes} aux tableaux de JavaScript, il suffit d'écrire\footnote{Implémenter un polyfill n'est toujours une tâche aisée, les spécifications pouvant être extrêmement complexes (Notamment celle de \texttt{fetch}), il est donc recommandé de laisser cette tâche à des bibliothèques spécialisées (comme \texttt{whatwg-fetch}).}

\noindent
\begin{minipage}{\linewidth}
\begin{lstlisting}
Array.prototype.includes = function (needle, startIndex = 0) {
	// find logic
}
\end{lstlisting}
\end{minipage}

\noindent
Il faut cependant faire attention, ajouter de nouvelles méthodes au prototype des objets standards est considéré comme une mauvaise pratique. Pour cause, les standards définissant ces objets sont continuellement en développement et de nouvelles méthodes, portant le même nom que celles ajoutées par les développeurs, peuvent être ajoutées dans les spécifications. Ces dernières entreraient en conflit avec celles des développeurs et casseraient des applications web déjà existantes. Le problème ne se pose cependant pas dans le cas des polyfills, car les méthodes ajoutées font déjà partie de standards et ne sont ajoutées que si le navigateur ne les supporte pas.

\subsubsection{Isomorphisme}

Avec l'apparition de node.js, un nouveau paradigme de programmation est né: L'isomorphisme. Son principe est simple: comme il est désormais possible d'utiliser le même langage --- JavaScript --- tant pour le client que le serveur, l'idée est de réutiliser le même code source dans ces deux environnements.

Ce paradigme est cependant confronté à un grand problème: La librairie standard proposée par les navigateurs et celle de node.js n'est pas identique. L'une est axée sur le contrôle d'une page web, l'autre est une librairie à utilité générale. La portée de l'isomorphisme est donc fort limitée, seul peut être réutilisé le code qui n'utilise pas la librairie standard ni de node.js, ni des navigateurs.

Les polyfills sont une des solutions possibles à ce problème, il existe des implémentations de certaines des \glspl{api} web en tant que librairie node.js et inversement. 

L'exemple le plus parlant est celui de \texttt{fetch}. \texttt{fetch} est un nouveau Living Standard développé par le \gls{whatwg} ayant pour objectif de remplacer \texttt{XMLHttpRequest} (Classe permettant d'effectuer des appels HTTP en JavaScript). Cette nouvelle \gls{api} est riche en fonctionnalités mais est conçue pour les navigateurs. \texttt{node-fetch} est un polyfill permettant d'utiliser \texttt{fetch} dans un environnement node.js. Dans les faits, il n'est pas plus qu'un adaptateur au module HTTP de la plateforme, convertissant les appels à fetch et ignorant les paramètres spécifiques aux navigateurs (tels que la cross-origin policy).

\subsection{Le futur des polyfills}

Actuellement, il n'est possible de créer des polyfills que pour le JavaScript. L'avenir est néanmoins prometteur à ce niveau pour deux raisons.

Premièrement, le \gls{w3c} a récemment créé la \emph{CSS-TAG Houdini Task Force} dont l'objectif est de créer de nouvelles technologies rendant le CSS extensible, permettant la création de polyfills pour les feuilles de styles.\autocite{houdini:1}

De plus, le consortium a également commencé le travail sur les Web Components, permettant de définir des composantes de page web réutilisables. Il serait notamment possible de définir de nouveaux éléments implémentant de nouveaux standards HTML non supportés par certains navigateurs. Il serait par exemple possible de polyfiller l'élément \texttt{<dialog>}, actuellement uniquement supporté par les navigateurs basés sur Chromium (Opéra et Chrome).
\autocite{custom_elements:1}
\footnote{Il existe déjà un polyfill pour \texttt{<dialog>}, ce dernier est utilisé dans le projet du stage, mais ce dernier n'utilise pas les web components et est donc limité à certains niveaux. Par exemple, le développeur souhaitant utiliser le polyfill est obligé de manuellement enregistrer chaque élément \texttt{<dialog>} du document auprès du polyfill afin que ce dernier y ajoute les fonctionnalités.}
\footnote{Dans la spécification actuelle, il ne serait pas possible de créer un Custom Element \texttt{<dialog>} car le nom d'un Custom Element doit obligatoirement contenir le caractère "-" pour empêcher la création d'éléments pouvant entrer en collision avec de futures balises HTML}